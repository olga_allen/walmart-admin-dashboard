﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Walmart_Admin_Dashboard.Helper;
using Walmart_Admin_Dashboard.Models;

namespace Walmart_Admin_Dashboard.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
     
        public ActionResult Index(int? cid)
        {
            if (cid == null)
            {
                cid = TempData["cid"]!= null ? Convert.ToInt16(TempData["Cid"]) : 1;
            }
            var str = cid == 1 ? "USPROD" : "USCERT";
            var gmstr = cid == 1 ? "GMPROD" : "GMCERT";

            //for NHM
            if (cid == 3)
            {}
                //var NHMGrandMasterStatus = CheckGrandMasterIsRunning(str);
            
            var fcGrandMasterStatus = CheckGrandMasterIsRunning(str);

            var gmGrandMasterStatus = CheckGrandMasterIsRunning(gmstr);
            
            var fcAnalysisDate = GetCompletedAnalysisDt(str);
            var gmAnalysisdate = GetCompletedAnalysisDt(gmstr);
            var nmAnalysisDate = GetCompleteStreamsCount_NHM(str);

            var processingCompleteStatus = new ProcessingCompleteStatusViewModel
            {
                FCCompletedAnalysisDt = fcAnalysisDate,
                GMCompletedAnalysisDt = gmAnalysisdate,
                CompleteStreamsCount_NHM = nmAnalysisDate
            };

            //for PROD check OSA data Extract status 
            if (cid == 1)
            {
                var osaExtractStatus = GetOsaExtractStatus(str);
                if (osaExtractStatus.AnalysisDate != fcAnalysisDate.AnalysisDate)
                {
                    osaExtractStatus.CurrentFileCount = 0;
                    osaExtractStatus.ExpectedFiles = 106;
                    osaExtractStatus.StatusCode = "N";
                }
                processingCompleteStatus.OsaExtractStatus = osaExtractStatus;
                
            }

            var model = new OscaDashboardViewModel
            {
                FCConStr = str,
                GMConStr = gmstr,
                ProcessingCompleteStatus = processingCompleteStatus,
                FCGrandMasterStatus = fcGrandMasterStatus,
                GMGrandMasterStatus = gmGrandMasterStatus,
                

            };
            ViewBag.Conn = str;
            TempData["cid"] = cid;
            return View("Index", model);
        }

        public IEnumerable<GrandMasterStatus> CheckGrandMasterIsRunning(string connStr)
        {
            var dataset = ReportingDataGetter.CheckGrandMasterIsRunning(connStr);
            var result = dataset.Tables[0].AsEnumerable().ToList();
            var list = new List<GrandMasterStatus>();

            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    var row = new GrandMasterStatus
                    {
                        JobName = item["JobName"] != System.DBNull.Value ? item["JobName"].ToString() : string.Empty,
                         OriginatingServer = item["OriginatingServer"] != System.DBNull.Value ? item["OriginatingServer"].ToString() : string.Empty,
                         RunStarted = item["RunStarted"] != System.DBNull.Value ? Convert.ToDateTime(item["RunStarted"]).ToString("dd/MM/yyyy HH:mm") : string.Empty,
                        IsEnabled = item["IsEnabled"] != System.DBNull.Value ? Convert.ToInt16(item["IsEnabled"]) == 1 ? "Enabled": "Disabled": "Unknown",
                        IsRunning = item["IsRunning"] != System.DBNull.Value ? Convert.ToInt16(item["IsRunning"]) == 1 ? "Running" : "Stopped" : "Unknown",

                    };
                    list.Add(row);
                }
            }
            return list;
        
        }

        public CompletedAnalysisDate GetCompletedAnalysisDt(string connStr)
        {
            var dataset = ReportingDataGetter.CurrentAnalysisDate(connStr);
            var result = dataset.Tables[0].AsEnumerable().ToList();

            if (result != null && result.Count == 1)
            {
                var row = new CompletedAnalysisDate
                {
                    AnalysisDate = result[0]["AnalysisDate"] != System.DBNull.Value ? Convert.ToDateTime(result[0]["AnalysisDate"]).ToString("dd/MM/yyyy") : string.Empty,
                    CompleteCount = result[0]["CompleteCount"] != System.DBNull.Value ? result[0]["CompleteCount"].ToString() : string.Empty,
                    StreamsCount = result[0]["StreamsCount"] != System.DBNull.Value ? result[0]["StreamsCount"].ToString() : string.Empty,
                    GMTransferCount = result[0].Table.Columns.Contains("GMTransferCount") && result[0]["GMTransferCount"] != System.DBNull.Value ? result[0]["GMTransferCount"].ToString() : string.Empty
                };
                return row;

            }
            return new CompletedAnalysisDate();
        }
        //GetCompleteStreamsCount_NHM(str)
        public CompletedAnalysisDate GetCompleteStreamsCount_NHM(string connStr)
        {
            var dataset = ReportingDataGetter.GetCompleteStreamsCount_NHM(connStr);
            var result = dataset.Tables[0].AsEnumerable().ToList();

            if (result != null && result.Count == 1)
            {
                var row = new CompletedAnalysisDate
                {
                    AnalysisDate = result[0]["AnalysisDate"] != System.DBNull.Value ? Convert.ToDateTime(result[0]["AnalysisDate"]).ToString("dd/MM/yyyy") : string.Empty,
                    CompleteCount = result[0]["CompleteCount"] != System.DBNull.Value ? result[0]["CompleteCount"].ToString() : string.Empty,
                    StreamsCount = result[0]["StreamsCount"] != System.DBNull.Value ? result[0]["StreamsCount"].ToString() : string.Empty
                };
                return row;

            }
            return new CompletedAnalysisDate();
        }


       public OsaDataExtractStatus GetOsaExtractStatus(string connStr)
        {
            var dataset = ReportingDataGetter.GetOsaExtractStatus(connStr);
            var result = dataset.Tables[0].AsEnumerable().ToList();

            if (result != null && result.Count ==1)
            {
                    var row = new OsaDataExtractStatus
                    {
                        AnalysisDate = result[0]["AnalysisDate"] != System.DBNull.Value ? Convert.ToDateTime(result[0]["AnalysisDate"]).ToString("dd/MM/yyyy") : string.Empty,
                        StatusCode = result[0]["StatusCode"] != System.DBNull.Value ? result[0]["StatusCode"].ToString() : string.Empty,
                        LastModifiedDateTime = result[0]["LastModifiedDateTime"] != System.DBNull.Value ? Convert.ToDateTime(result[0]["LastModifiedDateTime"]).ToString("dd/MM/yyyy HH:mm") : string.Empty,
                        ExpectedFiles = result[0]["ExpectedFiles"] != System.DBNull.Value ? Convert.ToInt32(result[0]["ExpectedFiles"]):0,
                        CurrentFileCount = result[0]["CurrentFileCount"] != System.DBNull.Value ? Convert.ToInt16(result[0]["CurrentFileCount"]) :0,
                    };
                   return row;
            }
            return new OsaDataExtractStatus();

        }


        


        //// POST: Dashboard/Delete/5
        //[HttpPost]
        //public ActionResult Delete(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add delete logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}
    }
}
