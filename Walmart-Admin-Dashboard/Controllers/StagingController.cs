﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Walmart_Admin_Dashboard.Helper;
using Walmart_Admin_Dashboard.Models;

namespace Walmart_Admin_Dashboard.Controllers
{
    public class StagingController : Controller
    {
        // GET: Staging
        public ActionResult Index(int? cid)
        {
            if (cid == null)
            {
                cid = TempData["cid"] != null ? Convert.ToInt16(TempData["Cid"]) : 1;
            }
            
            var str = cid == 1 ? "USPROD" : "USCERT";

            var dataset = GetStagingData(str, string.Empty, string.Empty, string.Empty, null,null, "FC");

            var taskHistory = dataset.Tables[3].AsEnumerable().ToList();
            var taskHistoryData = new List<TaskHistoryViewModel>();
            if (taskHistory != null)
            {
                taskHistoryData = GetTaskHistoryData(taskHistory).ToList();
            }

            var tasksInProgressData = new List<TasksInProgressViewModel>();
            var taskInProgress = dataset.Tables[2].AsEnumerable().ToList();
            if(taskInProgress!=null)
            {
                tasksInProgressData = GetTasksInProgressData(taskInProgress).ToList();
            }
            

            var model = new StagingDataModel()
            {
                TaskHistory = taskHistoryData,
                TasksInProgress = tasksInProgressData,
            };
            TempData["cid"] = cid;
            ViewBag.Conn = str;
            return View("Index", model);
    }

        public ActionResult ApplyFilter(string cid, string status, string taskDescr, string fileName, DateTime? taskStart, DateTime? taskEnd, string market)
        { 
            if (cid == null)
            {
                cid = TempData["cid"] != null ? TempData["Cid"].ToString() : "1";
            }

            var str = cid == "1" ? "USPROD" : "USCERT";
            var dataset = GetStagingData(str,status, taskDescr, fileName, taskStart, taskEnd, market);
            var taskHistory = dataset.Tables[3].AsEnumerable().ToList();
            var taskHistoryData = new List<TaskHistoryViewModel>();
            if (taskHistory != null)
            {
                taskHistoryData = GetTaskHistoryData(taskHistory).ToList();
            }
            var tasksInProgressData = new List<TasksInProgressViewModel>();
            var taskInProgress = dataset.Tables[2].AsEnumerable().ToList();
            if (taskInProgress != null)
            {
                tasksInProgressData = GetTasksInProgressData(taskInProgress).ToList();
            }


            var model = new StagingDataModel()
            {
                TaskHistory = taskHistoryData,
                TasksInProgress = tasksInProgressData,
            };
            TempData["cid"] = cid;
            ViewBag.Conn = str;
           // return View("Index", model);

            return PartialView("_StagingView", model);
        }
        private DataSet GetStagingData(string connStr, string status,string taskDescr,  string fileName, DateTime? taskStart, DateTime? taskEnd, string market)
        {
            return ReportingDataGetter.GetStagingQuery(connStr, status, taskDescr, fileName, taskStart, taskEnd, market);
        }
        public IEnumerable<TaskHistoryViewModel> GetTaskHistoryData(List<DataRow> result)
        {
            var list = new List<TaskHistoryViewModel>();
           
                foreach (var item in result)
                {

                    var row = new TaskHistoryViewModel
                    {
                        Market = item["Market"] != DBNull.Value ? item["Market"].ToString() : "FC",
                        TaskDescription = item["TaskDescription"] != System.DBNull.Value ? item["TaskDescription"].ToString() : string.Empty,
                        ProcMins = item["ProcMins"] != System.DBNull.Value ? Convert.ToInt32(item["ProcMins"]) : 0,
                        TaskID = item["TaskID"] != System.DBNull.Value ? item["TaskID"].ToString() : string.Empty,
                        FileName = item["FileName"] != System.DBNull.Value ? item["FileName"].ToString() : string.Empty,
                        ControlID = item["ControlID"] != System.DBNull.Value ? item["ControlID"].ToString() : string.Empty,
                        TaskStart = item["TaskStart"] != System.DBNull.Value ? Convert.ToDateTime(item["TaskStart"]).ToString("dd/MM/yyyy HH:mm") : string.Empty,
                        TaskEnd = item["TaskEnd"] != System.DBNull.Value ? Convert.ToDateTime(item["TaskEnd"]).ToString("dd/MM/yyyy HH:mm") : string.Empty,
                        StatusCode = item["StatusCode"] != System.DBNull.Value ? item["StatusCode"].ToString() : string.Empty,
                        AttemptNo = item["AttemptNo"] != System.DBNull.Value ? item["AttemptNo"].ToString() : string.Empty,
                    };
                    list.Add(row);
                }
            
            return list;
        }
        public IEnumerable<TasksInProgressViewModel> GetTasksInProgressData(List<DataRow> result)
        {
            var list = new List<TasksInProgressViewModel>();
           
                foreach (var item in result)
                {

                    var row = new TasksInProgressViewModel
                    {
                        TaskDescription = item["TaskDescription"] != System.DBNull.Value ? item["TaskDescription"].ToString() : string.Empty,
                        ProcMins = item["ProcMins"] != System.DBNull.Value ? Convert.ToInt32(item["ProcMins"]) : 0,
                        TaskID = item["TaskID"] != System.DBNull.Value ? item["TaskID"].ToString() : string.Empty,
                        TaskInstanceID = item["TaskInstanceID"] != System.DBNull.Value ? item["TaskInstanceID"].ToString() : string.Empty,
                        TaskStart = item["TaskStart"] != System.DBNull.Value ? Convert.ToDateTime(item["TaskStart"]).ToString("dd/MM/yyyy HH:mm") : string.Empty
                       
                    };
                    list.Add(row);
                }
            
            return list;
        }
        
    }
}