﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Walmart_Admin_Dashboard.Helper;
using Walmart_Admin_Dashboard.Models;

namespace Walmart_Admin_Dashboard.Controllers
{
    public class ProcessingController : Controller
    {
        // GET: Processing
        public ActionResult Index(int? cid)
        {
            if (cid == null)
            {
                cid = TempData["cid"] != null ? Convert.ToInt16(TempData["Cid"]) : 1;
            }
            var str = cid == 1 ? "USPROD" : "USCERT";
            var gmstr = cid == 1 ? "GMPROD" : "GMCERT";

           

            var fc = GetStillToProcess(str, "FC");
            var gm = GetStillToProcess(gmstr, "GM");
            var nm = GetStillToProcess(str,"NHM");

            var fcAnalysisDate = GetCompletedAnalysisDt(str);
            var gmAnalysisdate = GetCompletedAnalysisDt(gmstr);
            var nmAnalysisDate = GetCompleteStreamsCount_NHM(str);

            var processingCompleteStatus = new ProcessingCompleteStatusViewModel
            {
                FCCompletedAnalysisDt = fcAnalysisDate,
                GMCompletedAnalysisDt = gmAnalysisdate,
                CompleteStreamsCount_NHM = nmAnalysisDate
            };
            var model = new Walmart_Admin_Dashboard.Models.OscaProcessViewModel()
                {
                    FCStillToProcess = fc,
                    GMStillToProcess = gm,
                    NHMStillToProcess = nm,
                    ProcessingCompleteStatus = processingCompleteStatus

                };
            //for PROD check OSA data Extract status and GM copy over
            if (cid==1)
            {
                var osaExtractStatus = GetOsaExtractStatus(str);
                if (osaExtractStatus.AnalysisDate != fcAnalysisDate.AnalysisDate)
                {
                    osaExtractStatus.CurrentFileCount = 0;
                    osaExtractStatus.ExpectedFiles = 106;
                }
                processingCompleteStatus.OsaExtractStatus = osaExtractStatus;

                var gmCopy = GetGMCopyover(0, str);
                model.GMCopyover = gmCopy;
            }
           
           
            

            ViewBag.Conn = str;
            TempData["cid"] = cid;
            return View("Index", model);
        }
        public IEnumerable<StillToProcessViewModel> GetStillToProcess(string connStr, string market)
        {
            var list = new List<StillToProcessViewModel>();
            
            var dataset = ReportingDataGetter.ReturnStillToProcessData(connStr, market);
            var result = dataset.Tables[0].AsEnumerable().ToList();

            if (result != null)
            {
                foreach (var item in result)
                {

                    var row = new StillToProcessViewModel
                    {
                        ControlID = item["ControlID"].ToString(),
                        TimeBucket = item["TimeBucket"].ToString(),
                        StreamID = item["StreamID"].ToString(),
                        AnalysisDate = Convert.ToDateTime(item["AnalysisDate"]).ToShortDateString(),
                        ControlRunStartDateTime = item["ControlRunStartDateTime"] != System.DBNull.Value ? item["ControlRunStartDateTime"].ToString() : string.Empty,//"dd/MM/yyyy HH:mm"
                        ControlStageStartDateTime = item["ControlStageStartDateTime"] != System.DBNull.Value ? item["ControlStageStartDateTime"].ToString() : string.Empty,
                        ControlStageEndDateTime = item["ControlStageEndDateTime"] != System.DBNull.Value ? item["ControlStageEndDateTime"].ToString() : string.Empty,
                        ControlExtractStartDateTime = item["ControlExtractStartDateTime"] != System.DBNull.Value ? item["ControlExtractStartDateTime"].ToString() : string.Empty,
                        ControlExtractEndDateTime = item["ControlExtractEndDateTime"] != System.DBNull.Value ? Convert.ToDateTime(item["ControlExtractEndDateTime"]).ToString() : string.Empty,
                        Duration = item["Duration"] != System.DBNull.Value ? Convert.ToInt32(item["Duration"]) : 0
                    };
                    list.Add(row);
                }
            }
            return list;
        }
        public IEnumerable<GMCopyoverViewModel> GetGMCopyover(int showHistory, string connStr)
        {
            var list = new List<GMCopyoverViewModel>();
            var dataset = ReportingDataGetter.GetGMReportingCopyover(showHistory, connStr);
            var result = dataset.Tables[0].AsEnumerable().ToList();

            if (result != null)
            {
                foreach (var item in result)
                {

                    var row = new GMCopyoverViewModel
                    {
                        AnalysisDate = item["AnalysisDate"] != System.DBNull.Value ? Convert.ToDateTime(item["AnalysisDate"]).ToShortDateString() : string.Empty,
                        TableName = item["TableName"] != System.DBNull.Value ? item["TableName"].ToString() : string.Empty,//"dd/MM/yyyy HH:mm"
                        AvgTransferDuration = item["AvgTransferDuration"] != System.DBNull.Value ? Convert.ToInt32(item["AvgTransferDuration"]) : 0,
                        NumComplete = item["NumComplete"] != System.DBNull.Value ? Convert.ToInt32(item["NumComplete"]) : 0,
                        MainlandComplete = item["MainlandComplete"] != System.DBNull.Value ? item["MainlandComplete"].ToString() : string.Empty,
                        Complete159 = item["Complete159"] != System.DBNull.Value ? item["Complete159"].ToString() : string.Empty,
                        Complete160 = item["Complete160"] != System.DBNull.Value ? item["Complete160"].ToString() : string.Empty

                    };
                    list.Add(row);
                }
            }
            return list;
        }
        public ActionResult LoadGMCopyover(string cid, bool chk)
        {
            var conStr = !string.IsNullOrEmpty(cid) ? cid == "1" ? "USPROD" : "USCERT" : "USPROD";
            var isChecked = chk ? 1 : 0;
            var gmCopy = GetGMCopyover(isChecked, conStr);
            var model = new List<GMCopyoverViewModel>();
            model = gmCopy.ToList();
            return PartialView("_GMCopyOver", model);
        }
        public CompletedAnalysisDate GetCompletedAnalysisDt(string connStr)
        {
            var dataset = ReportingDataGetter.CurrentAnalysisDate(connStr);
            var result = dataset.Tables[0].AsEnumerable().ToList();

            if (result != null && result.Count == 1)
            {
                var row = new CompletedAnalysisDate
                {
                    AnalysisDate = result[0]["AnalysisDate"] != System.DBNull.Value ? Convert.ToDateTime(result[0]["AnalysisDate"]).ToString("dd/MM/yyyy") : string.Empty,
                    CompleteCount = result[0]["CompleteCount"] != System.DBNull.Value ? result[0]["CompleteCount"].ToString() : string.Empty,
                    StreamsCount = result[0]["StreamsCount"] != System.DBNull.Value ? result[0]["StreamsCount"].ToString() : string.Empty,
                    GMTransferCount = result[0].Table.Columns.Contains("GMTransferCount") && result[0]["GMTransferCount"] != System.DBNull.Value ? result[0]["GMTransferCount"].ToString() : string.Empty

                };
                return row;

            }
            return new CompletedAnalysisDate();
        }
        public OsaDataExtractStatus GetOsaExtractStatus(string connStr)
        {
            var dataset = ReportingDataGetter.GetOsaExtractStatus(connStr);
            var result = dataset.Tables[0].AsEnumerable().ToList();

            if (result != null && result.Count == 1)
            {
                var row = new OsaDataExtractStatus
                {
                    AnalysisDate = result[0]["AnalysisDate"] != System.DBNull.Value ? Convert.ToDateTime(result[0]["AnalysisDate"]).ToString("dd/MM/yyyy") : string.Empty,
                    StatusCode = result[0]["StatusCode"] != System.DBNull.Value ? result[0]["StatusCode"].ToString() : string.Empty,
                    LastModifiedDateTime = result[0]["LastModifiedDateTime"] != System.DBNull.Value ? Convert.ToDateTime(result[0]["LastModifiedDateTime"]).ToString("dd/MM/yyyy HH:mm") : string.Empty,
                    ExpectedFiles = result[0]["ExpectedFiles"] != System.DBNull.Value ? Convert.ToInt32(result[0]["ExpectedFiles"]) : 0,
                    CurrentFileCount = result[0]["CurrentFileCount"] != System.DBNull.Value ? Convert.ToInt16(result[0]["CurrentFileCount"]) : 0,
                };
                return row;
            }
            return new OsaDataExtractStatus();

        }

        public CompletedAnalysisDate GetCompleteStreamsCount_NHM(string connStr)
        {
            var dataset = ReportingDataGetter.GetCompleteStreamsCount_NHM(connStr);
            var result = dataset.Tables[0].AsEnumerable().ToList();

            if (result != null && result.Count == 1)
            {
                var row = new CompletedAnalysisDate
                {
                    AnalysisDate = result[0]["AnalysisDate"] != System.DBNull.Value ? Convert.ToDateTime(result[0]["AnalysisDate"]).ToString("dd/MM/yyyy") : string.Empty,
                    CompleteCount = result[0]["CompleteCount"] != System.DBNull.Value ? result[0]["CompleteCount"].ToString() : string.Empty,
                    StreamsCount = result[0]["StreamsCount"] != System.DBNull.Value ? result[0]["StreamsCount"].ToString() : string.Empty
                };
                return row;

            }
            return new CompletedAnalysisDate();
        }

    }
}