﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Walmart_Admin_Dashboard.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SelectMarket(int cid)
        {
            // var str = cid == 1 ? "USPROD" : "FCCERT";
            TempData["cid"] = cid;
            return RedirectToAction("Index", "Dashboard", new { cid = cid });
           
        }

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
    }
}