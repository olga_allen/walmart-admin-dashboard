# Introduction

**NugetAppPackager** is used to package your .Net applications as nuget package from your continuous integration/automated build process.

To deploy .Net applications like web application, console application, windows service application etc via **oneops** we are using nuget package.

The current features of NugetAppPackager are:
- Creates nuspec file which is required for creating nuget package.
- Versioning of your package using git commit, major version default value is 1 and minor version default value is 0.
- Adding of machine keys to your project web.config file, which is required if you want to deploy your ASP.net web application in web farm.

NuGet packages are basically ZIP files with extra metadata describing the contents of the package. They follow the Open Packaging Conventions, and use the .nupkg file extension. You can learn more about NuGet and NuGet Packages on the official NuGet website.

NuGet is a good choice for packaging your applications, for the following reasons:

- NuGet packages have rich metadata, such as versioning, release notes, and author information
- Many tools exist to make creating and publishing NuGet packages easy
- NuGet packages can be consumed via a feed, so other applications can easily query the available packages
- Developer familiarity

NuGet was originally designed for packaging up open-source code libraries for developers to use in Visual Studio. And it also happens to be the perfect format for packaging applications that you want to deploy.

# Building application - .Net Framework

In CI process when you build your project using msbuild add the below parameter and build your project in release mode.

    /p:DeployOnBuild=True /p:Configuration=Release

# Publishing application - .Net Core

In CI process, when you publish your asp.net core/asp.net core webapi project using dotnet(cli) add below parameter. This will publish the code in release mode and also publish the output in publish folder. NugetAppPackager will use this folder to create the nuspec file.

    -c Release -o publish

# Nuspec file

A `.nuspec` file describes the contents of your NuGet package. **NugetAppPackager** automatically creates one depending on the output files of your build.

Here is an example of the .nuspec file contents:

	<?xml version="1.0"?>
	<package xmlns="http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd">
	  <metadata>
	    <id>Sample.Web</id>
	    <title>Your Web Application</title>
	    <version>1.0.0</version>
	    <authors>Your name</authors>
	    <owners>Your name</owners>
	    <licenseUrl>http://yourcompany.com</licenseUrl>
	    <projectUrl>http://yourcompany.com</projectUrl>
	    <requireLicenseAcceptance>false</requireLicenseAcceptance>
	    <description>A sample project</description>
	    <releaseNotes>This release contains the following changes...</releaseNotes>
	  </metadata>
	</package>

# Versioning

Nuget packages have version numbers. **NugetAppPackager** uses git commit and git commit count to version your nuget package

    Format: <project-name>-1.0.<git commit>.<git commit count>.nupkg
    Example: CCMSampleWebApp.1.0.122111371.18.nupkg

# Machine keys

Machines keys are required when you want to deploy your ASP.net web application in a web farm. These will be added to your web.config of your project. To add machine keys to the web.config pass the below parameter.

    ./NugetAppPackager -generateMachineKeys true

# Generate a differently named package
Generally all packages generated are named based on the project name from which the package is being built. However there are cases where you want to name your package to a different name. Now we do offer the option to do that. Just pass the below parameter

	./NugetAppPackager -idName "MyPackage" 

This will generate a package named "MyPackage.123.nupkg" where 123 is the version number that is automatically generated

# How your build step should look like

Add a build step in your build/CI process as powershell script. Nuget.exe should be present in your build agent or in your project directory as this is required for downloading the NugetAppPackager from the nexus server. Add the below powershell script snippet in your build step, please make sure the path of the nuget.exe is correct and Working folder is set to your project directory.

    $cd = [System.IO.Directory]::GetCurrentDirectory()

    ..\.nuget\NuGet.exe install NugetAppPackager -source http://oser501022.wal-mart.com:8081/service/local/nuget/nuget-shared/ -outputdirectory $cd -ExcludeVersion -NoCache

    $np = $cd + '\NugetAppPackager\NugetAppPackager.ps1'
    Copy-Item $np -destination $cd -Force

    .\NugetAppPackager.ps1
