Param(
  $majorVersion = 1,
  $minorVersion = 0,
  $release = 'Release',
  $authors = $Env:UserName,
  $generateMachineKeys = $false,
  $idName =  ""
)

$projectFileName = (Get-ChildItem | ? {$_.Name -like "*.*proj"} | select Name).Name

if (![System.String]::IsNullOrEmpty($projectFileName))
{
    $currentDirectory = (Get-Item -Path ".\" -Verbose).FullName
    $projectName = if ([string]::IsNullOrWhiteSpace($idName)) {(Get-Item $projectFileName).BaseName} else { $idName}
    $fileName = ''

    if($release -eq 'Release'){
       $fileName = '{0}\{1}.nuspec' -f $currentDirectory, $projectName
    } else {
       $fileName = '{0}\{1}.{2}.nuspec' -f $currentDirectory, $projectName, $release
    }


    $isWebApplication = Test-Path $currentDirectory\* -include web.config
    $isDotNetCoreWebApp = Test-Path $currentDirectory\wwwroot
    $isDotNetCoreApp = Test-Path $currentDirectory\publish


    if($isDotNetCoreWebApp -or $isDotNetCoreApp)
    {
      $projectDirectory = '{0}\publish' -f $currentDirectory
    }
    elseif($isWebApplication)
    {
      $projectDirectory = '{0}\obj\{1}\Package\PackageTmp' -f $currentDirectory, $release
      $webConfig = '{0}\Web.config' -f $projectDirectory
    }
    else
    {
      $projectDirectory = '{0}\bin\{1}' -f $currentDirectory, $release
    }


    try {
      if(Test-Path -Path 'C:\Program Files (x86)\Git\bin')
      {
        $env:Path += ";C:\Program Files (x86)\Git\bin"

      }
      $gitCommitCount = git rev-list --all --count
  	  if ($gitCommitCount -eq $null)
  	  {
  		  $gitCommitCount = 0
  	  }
    }
    catch {
      $gitCommitCount = 0
    }
    $version = '{0}.{1}.{2}' -f $majorVersion, $minorVersion, $gitCommitCount

    if (Test-Path -Path .\$fileName )
    {
        Remove-Item -Path .\$fileName -Force

    }

    $generate_keys = @"
    using System;
    using System.Text;
    using System.Security;
    using System.Security.Cryptography;

    public static class MachineKeys {
      public static string Generate(int length) {
        byte[] buff = new byte[length/2];
        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        rng.GetBytes(buff);
        StringBuilder sb = new StringBuilder(length);
        for (int i=0; i<buff.Length; i++)
          sb.Append(string.Format("{0:X2}", buff[i]));
        return sb.ToString();
      }
    }
"@

    if($generateMachineKeys -eq $true) {
      Add-Type -TypeDefinition $generate_keys
      $validationKey = [MachineKeys]::Generate(128)
      $decryptionKey = [MachineKeys]::Generate(64)

      if(!(select-string -Path $webConfig -pattern "machineKey" -SimpleMatch)) {
          [xml]$xml = get-content $webConfig
          $newElement = $xml.CreateElement('machineKey')
          $newElement.SetAttribute("validationKey", $validationKey)
          $newElement.SetAttribute("decryptionKey", $decryptionKey)
          $newElement.SetAttribute("validation", "SHA1")
          $newElement.SetAttribute("decryption", "AES")
          $xml.configuration.'system.web'.AppendChild($newElement)
          $xml.Save($webConfig)
          echo "Encryption keys added to $webConfig"
      }
      else
      {
          echo "There are already Encryption keys in the $webConfig."
      }
    }

    # Create The Document
    $encoding = [System.Text.Encoding]::UTF8
    $XmlWriter = New-Object System.XMl.XmlTextWriter($fileName,$encoding)


    # Set The Formatting
    $xmlWriter.Formatting = "Indented"
    $xmlWriter.Indentation = "4"

    # Write the XML Decleration
    $xmlWriter.WriteStartDocument()
    #Write Root Element
    $xmlWriter.WriteStartElement("package")
    # Write the Document
    $xmlWriter.WriteStartElement("metadata")

    if($release -eq 'Release'){
       $xmlWriter.WriteElementString("id", $projectName)
       $xmlWriter.WriteElementString("description", 'The ' + $projectName+ ' deployment package, built on ' + [System.DateTime]::Now.ToShortDateString())
       $xmlWriter.WriteElementString("releaseNotes", 'This pack is used to deploy ' + $projectName)
    } else {
       $xmlWriter.WriteElementString("id", $projectName+"."+$release)
       $xmlWriter.WriteElementString("description", 'The ' + $projectName+"."+$release + ' deployment package, built on ' + [System.DateTime]::Now.ToShortDateString())
       $xmlWriter.WriteElementString("releaseNotes", 'This pack is used to deploy ' + $projectName+"."+$release)
    }

    $xmlWriter.WriteElementString("version", $version)
    $xmlWriter.WriteElementString("authors", $authors)
    $xmlWriter.WriteElementString("requireLicenseAcceptance", "false")
    $xmlWriter.WriteElementString("copyright", 'copyright ' + [System.DateTime]::Now.Year)
    $xmlWriter.WriteEndElement() # <-- Closing metadata

    $xmlWriter.WriteStartElement("files")
    Get-ChildItem -Path $projectDirectory | foreach {
        $xmlWriter.WriteStartElement("file")
        if ($_.Attributes -eq "Directory")
        {

          $xmlWriter.WriteAttributeString("src", $projectDirectory + '\' + $_.Name + '\**')
          $xmlWriter.WriteAttributeString("target", $_.Name)
        }
        else {
          $xmlWriter.WriteAttributeString("src", $projectDirectory + '\' + $_.Name)
          $xmlWriter.WriteAttributeString("target", ".")

        }
        $xmlWriter.WriteEndElement()
    }
    $xmlWriter.WriteEndElement() # Closing files
    $xmlWriter.WriteEndElement() # Closing package
    # End the XML Document
    $xmlWriter.WriteEndDocument()
    # Finish The Document
    $xmlWriter.Finalize
    $xmlWriter.Flush | out-null
    $xmlWriter.Close()

    echo "Succesfully created nupsec file $fileName"

}
else
{
    echo "NO .csproj file found."
}
