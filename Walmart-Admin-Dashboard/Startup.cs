﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Walmart_Admin_Dashboard.Startup))]
namespace Walmart_Admin_Dashboard
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
