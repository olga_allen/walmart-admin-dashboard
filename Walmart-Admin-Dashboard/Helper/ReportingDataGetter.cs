﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Walmart_Admin_Dashboard.DashBoardService;

namespace Walmart_Admin_Dashboard.Helper
{
    public class ReportingDataGetter
    {
        private readonly Walmart_Admin_Dashboard.DashBoardService.IDataDAL _dalService;
        //private readonly IErrorHandler _errorHandler;


        public ReportingDataGetter(IDataDAL dalService)//IErrorHandler errorHandler, )
        {
            //_errorHandler = errorHandler;
            _dalService = dalService;

        }

        static string _connstr;
        public static string connStr
        {
            get
            {
                return _connstr;
            }
            set
            {
                _connstr = value;
            }
        }
        public static SqlConnection GetSqlConnection(string conStr)
        {
            string connStr = System.Configuration.ConfigurationManager.ConnectionStrings[conStr].ConnectionString;

            SqlConnection MyConnection = new SqlConnection(connStr);
            return MyConnection;
        }
        public static DataSet ReturnStillToProcessData(string conStr, string market)
        {
            var connection = GetSqlConnection(conStr);
            var spName = "Availability.StillToProcess";
            if (market == "NHM")
            {
                spName = "Availability.StillToProcess_NHM";
            }
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(spName, connection);

            MyDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            MyDataAdapter.SelectCommand.CommandTimeout = 2400;
            //if (AllData == 1)
            //{
            //    MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@all", SqlDbType.TinyInt));
            //    MyDataAdapter.SelectCommand.Parameters["@all"].Value = 1;
            //}

            DataSet DS = new DataSet();
            try
            {
                MyDataAdapter.Fill(DS, "table");
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }

            return DS;

        }

      
        public static DataSet GetGMReportingCopyover(int showHistory, string conStr)
        {
            var connection = GetSqlConnection(conStr);
            var spName = "Availability.GMReportingCopyover";
            var list1 = new Dictionary<string, object>
               {
                   {"@all", showHistory}
               };
            return ExecStoredProc(connection, spName, list1 );
        }

        private static DataSet ExecStoredProc( SqlConnection connection, string spName,  Dictionary<string, object> parms)
        {
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(spName, connection);

            MyDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            MyDataAdapter.SelectCommand.CommandTimeout = 2400;

            if (parms!=null)
            { 
                foreach (KeyValuePair<string, object> param in parms)
                {
                    MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter(param.Key, SqlDbType.TinyInt));
                    MyDataAdapter.SelectCommand.Parameters[param.Key].Value = param.Value;
                }
            }
            
            DataSet DS = new DataSet();
            try
            {
                MyDataAdapter.Fill(DS, "table");
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }
            return DS;
        }

        public static DataSet CurrentAnalysisDate(string conStr)
        {
            var connection = GetSqlConnection(conStr);
            var spName = "[Availability].[GetLastCompleteAnalysisDate]";
            return ExecStoredProc(connection, spName, null);
        }

        public static DataSet GetCompleteStreamsCount_NHM(string conStr)
        {
            var connection = GetSqlConnection(conStr);
            var spName = "[Availability].[GetCompleteStreamsCount_NHM]";
            return ExecStoredProc(connection, spName, null);
        }
       
        public static DataSet CheckGrandMasterIsRunning(string conStr)
        {
            var connection = GetSqlConnection(conStr);
            var spName = "Availability.CheckGrandMasterIsRunning";
            return ExecStoredProc(connection, spName, null);
        }

        public static DataSet GetOsaExtractStatus(string conStr)
        {
            var connection = GetSqlConnection(conStr);
            var spName = "Availability.GetOsaExtractStatus";
            return ExecStoredProc(connection, spName, null);
        }

        public static DataSet GetStagingQuery(string conStr, string statusCode, string taskDescr, string fileName, 
            DateTime? taskStart, DateTime? taskEnd, string market)
        {
            var connection = GetSqlConnection(conStr);
            var spName = "Availability.WhatStagingDoing";
            if (market == "NHM")
            {
                spName = "Availability.WhatStagingDoing_NHM";
            }
            if (market == "GM")
            {
                spName = "Availability.WhatStagingDoing_GM";
                connection = GetSqlConnection("GMPROD");
            }
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(spName, connection);

            MyDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            MyDataAdapter.SelectCommand.CommandTimeout = 2400;
            if (!string.IsNullOrEmpty(statusCode))
            {
                MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@StatusCode", SqlDbType.Char));
                MyDataAdapter.SelectCommand.Parameters["@StatusCode"].Value = statusCode;
            }
            //@TaskDescription
            if (!string.IsNullOrEmpty(taskDescr))
            {
                MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@TaskDescription", SqlDbType.VarChar));
                MyDataAdapter.SelectCommand.Parameters["@TaskDescription"].Value = taskDescr;
            }
            if (!string.IsNullOrEmpty(fileName))
            {
                MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@FileName", SqlDbType.NVarChar));
                MyDataAdapter.SelectCommand.Parameters["@FileName"].Value = fileName;
            }
            if (taskStart!= null)
            {
                MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@TaskStartDate", SqlDbType.DateTime));
                MyDataAdapter.SelectCommand.Parameters["@TaskStartDate"].Value = taskStart;
            }
            if (taskEnd != null)
            {
                MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@TaskEndDate", SqlDbType.DateTime));
                MyDataAdapter.SelectCommand.Parameters["@TaskEndDate"].Value = taskEnd;
            }
            DataSet DS = new DataSet();
            try
            {
                MyDataAdapter.Fill(DS, "table");
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }
            return DS;
        }


           
        }

    //public DataSet GetReportDataWithMultiParms(string stroreProcName, Dictionary<string, object> parms, int timeout = 1000)
    //{
    //    Database objDB = new SqlDatabase(ConnectionString);
    //    StringBuilder sb = new StringBuilder();
    //    using (DbCommand objcmd = objDB.GetStoredProcCommand(stroreProcName))
    //    {
    //        //StringBuilder sb = new StringBuilder();
    //        try
    //        {
    //            foreach (KeyValuePair<string, object> param in parms)
    //            {
    //                DbParameter dbParm = objcmd.CreateParameter();
    //                dbParm.ParameterName = param.Key;
    //                dbParm.Value = param.Value;
    //                var paramvalue = param.Value == null ? string.Empty : param.Value.ToString();
    //                sb.Append(string.Format("Parameter Name : {0} , Value : {1}", param.Key.ToString(), paramvalue + Environment.NewLine));
    //                objcmd.Parameters.Add(dbParm);
    //            }
    //            objcmd.CommandTimeout = timeout;
    //            Logger.Info(String.Format("Executing {0}  with {1}", stroreProcName, sb.ToString()));
    //            return objDB.ExecuteDataSet(objcmd);
    //        }
    //        catch (Exception ex)
    //        {
    //            ex.Data.Add("UserData", String.Format("Executing {0}  with {1}", stroreProcName, sb.ToString()));
    //            Logger.Error(String.Format("Error in executing {0}  with {1}", stroreProcName, sb.ToString()), ex);
    //            throw ex;
    //        }
    //    }

    //}
}