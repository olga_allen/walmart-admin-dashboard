﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Walmart_Admin_Dashboard.DashBoardService
{
    public interface IDataDAL
    {
        DataSet GetReportData(string connStr, string storeProcName, object[] parms);
        int ExecuteQuery(string connStr, string storeProcName, object[] parms);
        DataSet GetReportDataWithMultiParms(string connStr, string storeProcName, Dictionary<string, object> parms, int timeout = 1000);

        DataSet GetDataSet(string connStr, string storeProcName, List<SqlParameter> parms, int timeout = 1000);
    }
    
}