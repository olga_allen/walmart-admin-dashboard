﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Walmart_Admin_Dashboard.DashBoardService
{
    public class DataDAL : IDataDAL
    {
        public DataSet GetReportData( string connStr, string stroreProcName, object[] parms)
        {
            try
            {
                var ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connStr].ConnectionString;
                Microsoft.Practices.EnterpriseLibrary.Data.Database objDB = new SqlDatabase(ConnectionString);
                DbCommand objcmd = null;
                if (parms == null)
                {
                    objcmd = objDB.GetStoredProcCommand(stroreProcName);
                }
                else
                {
                    objcmd = objDB.GetStoredProcCommand(stroreProcName, parms);
                }
                return objDB.ExecuteDataSet(objcmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataSet GetReportDataWithMultiParms(string connStr, string stroreProcName, Dictionary<string, object> parms, int timeout = 1000)
        {
            var ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connStr].ConnectionString;
            Microsoft.Practices.EnterpriseLibrary.Data.Database objDB = new SqlDatabase(ConnectionString);
            StringBuilder sb = new StringBuilder();
            using (DbCommand objcmd = objDB.GetStoredProcCommand(stroreProcName))
            {
                //StringBuilder sb = new StringBuilder();
                try
                {
                    foreach (KeyValuePair<string, object> param in parms)
                    {
                        DbParameter dbParm = objcmd.CreateParameter();
                        dbParm.ParameterName = param.Key;
                        dbParm.Value = param.Value;
                        var paramvalue = param.Value == null ? string.Empty : param.Value.ToString();
                        sb.Append(string.Format("Parameter Name : {0} , Value : {1}", param.Key.ToString(), paramvalue + Environment.NewLine));
                        objcmd.Parameters.Add(dbParm);
                    }
                    objcmd.CommandTimeout = timeout;
                    // Logger.Info(String.Format("Executing {0}  with {1}", stroreProcName, sb.ToString()));
                    return objDB.ExecuteDataSet(objcmd);
                }
                catch (Exception ex)
                {
                    ex.Data.Add("UserData", String.Format("Executing {0}  with {1}", stroreProcName, sb.ToString()));
                    // Logger.Error(String.Format("Error in executing {0}  with {1}", stroreProcName, sb.ToString()), ex);
                    throw ex;
                }
            }
        }
        public DataSet GetDataSet(string connStr, string commandText, List<SqlParameter> parms = null, int timeout = 1000)
        {
            var ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connStr].ConnectionString;

            var ds = new DataSet();
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(commandText, connection) { CommandType = CommandType.StoredProcedure, CommandTimeout = timeout })
                {
                    if (parms != null)
                        foreach (var p in parms)
                        {
                            command.Parameters.Add(p);

                        }
                    try
                    {
                        connection.Open();
                        using (var dataAdaptor = new SqlDataAdapter(command))
                            dataAdaptor.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        ex.Data.Add("UserData", String.Format(" While Executing Proc  {0}", commandText));
                        throw ex;
                    }
                    finally
                    {
                        // command.Parameters.Clear();
                        connection.Close();
                    }
                    return ds;
                }
            }
        }
        

        public int ExecuteQuery(string connStr, string stroreProcName, object[] parms)
        {
           var ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connStr].ConnectionString;
            Microsoft.Practices.EnterpriseLibrary.Data.Database objDB = new SqlDatabase(ConnectionString);

            using (DbCommand objcmd = objDB.GetStoredProcCommand(stroreProcName, parms))
            {
                try
                {
                    objcmd.CommandTimeout = 1000;
                    foreach (System.Data.SqlClient.SqlParameter parameter in objcmd.Parameters)
                    {
                        if (parameter.SqlDbType != SqlDbType.Structured)
                        {
                            continue;
                        }
                        string name = parameter.TypeName;
                        int index = name.IndexOf(".");
                        if (index == -1)
                        {
                            continue;
                        }
                        name = name.Substring(index + 1);
                        if (name.Contains("."))
                        {
                            parameter.TypeName = name;
                        }
                    }
                    return objDB.ExecuteNonQuery(objcmd);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        
    }
}