﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Walmart_Admin_Dashboard.Models
{
    public class OscaProcessViewModel
    {
        public IEnumerable<StillToProcessViewModel> FCStillToProcess { get; set; }
        public IEnumerable<StillToProcessViewModel> GMStillToProcess { get; set; }
        public IEnumerable<StillToProcessViewModel> NHMStillToProcess { get; set; }
        public IEnumerable<GMCopyoverViewModel> GMCopyover { get; set; }
        public  ProcessingCompleteStatusViewModel ProcessingCompleteStatus { get; set; }

    }
    public class ProcessingCompleteStatusViewModel
    {
        public CompletedAnalysisDate FCCompletedAnalysisDt { get; set; }
        public CompletedAnalysisDate GMCompletedAnalysisDt { get; set; }
        public CompletedAnalysisDate CompleteStreamsCount_NHM { get; set; }
        public OsaDataExtractStatus OsaExtractStatus { get; set; }
    }
    public class OscaDashboardViewModel
    {
        public int conStrId { get; set; }
        public string FCConStr { get; set; }
        public string GMConStr { get; set; }
        public ProcessingCompleteStatusViewModel ProcessingCompleteStatus { get; set; }
        public IEnumerable<GrandMasterStatus> FCGrandMasterStatus { get; set; }
        public IEnumerable<GrandMasterStatus> GMGrandMasterStatus { get; set; }
       
    }
    public class OsaDataExtractStatus
    {
        public string AnalysisDate { get; set; }
        public int? ExpectedFiles { get; set; }
        public int? CurrentFileCount { get; set; }
        public string StatusCode { get; set; }
        public string LastModifiedDateTime { get; set; }
    }

    public class GrandMasterStatus
    {
        public string JobName { get; set; }
        public string OriginatingServer { get; set; }
        public string RunStarted { get; set; }
        public string IsEnabled { get; set; }
        public string IsRunning { get; set; }
        public string CurrentRunTime { get; set; }
        
    }

        public class CompletedAnalysisDate
    {
        public string AnalysisDate { get; set; }
        public string CompleteCount { get; set; }
        public string StreamsCount { get; set; }
        public string GMTransferCount { get; set; }
    }
    public class StillToProcessViewModel
    {
        public string ControlID { get; set; }
        public string TimeBucket { get; set; }
        public string StreamID { get; set; }
        public string AnalysisDate { get; set; }
        public string ControlRunStartDateTime { get; set; }
        public string ControlStageStartDateTime { get; set; }
        public string ControlStageEndDateTime { get; set; }
        public string ControlExtractStartDateTime { get; set; }
        public string ControlExtractEndDateTime { get; set; }
        public int Duration { get; set; }
        
    }
    public class GMCopyoverViewModel
    {
        public string AnalysisDate { get; set; }
        public string TableName { get; set; }
        public int AvgTransferDuration { get; set; }
        public int NumComplete { get; set; }
        public string MainlandComplete { get; set; }
        public string Complete159 { get; set; }
        public string Complete160 { get; set; }
        
    }
}