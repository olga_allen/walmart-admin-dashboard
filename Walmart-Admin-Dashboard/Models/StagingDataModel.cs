﻿using System.Collections.Generic;

namespace Walmart_Admin_Dashboard.Models
{
    public class StagingDataModel
    {
        public IEnumerable<TasksInProgressViewModel> TasksInProgress { get; set; }
        public IEnumerable<TaskHistoryViewModel> TaskHistory { get; set; }
    }
    public class TaskHistoryViewModel
    {
        public string Market { get; set; }
        public string TaskDescription { get; set; }
        public string TaskInstanceID { get; set; }
        public int ProcMins { get; set; }
        public string TaskID { get; set; }
        public string FileName { get; set; }
        public string ControlID { get; set; }
        public string TaskStart { get; set; }
        public string TaskEnd { get; set; }
        public string StatusCode { get; set; }
        public string AttemptNo { get; set; }
    }
    public class TasksInProgressViewModel
    {
        public string TaskDescription { get; set; }
        public string TaskInstanceID { get; set; }
        public string TaskID { get; set; }
        public string TaskStart { get; set; }
        public int ProcMins { get; set; }
    }

   




    }